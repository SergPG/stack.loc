@extends(env('THEME').'.layouts.index')

@section('content') 
    
		@section('services_top_main') 
		   @include(env('THEME').'.services.services_top_main')  		 	
		@show
			
		@section('testimonial') 
		   @include(env('THEME').'.layouts.testimonial')  		 	
		@show
		
		@section('section_bottom') 
		   @include(env('THEME').'.layouts.section_bottom')  		 	
		@show

@endsection

		



