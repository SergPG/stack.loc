@extends(env('THEME').'.layouts.index')

@section('content') 
    
		@section('portfolio_products') 
		   	@include(env('THEME').'.portfolios.portfolio_products') 		 	
		@show
			
		@section('testimonial') 
		   @include(env('THEME').'.layouts.testimonial')  		 	
		@show
		
		@section('section_bottom') 
		   @include(env('THEME').'.layouts.section_bottom')  		 	
		@show

@endsection

		



