
           
	<!--  Products Portfolio --> 
		@if(isset($products) && is_object($products) && (count($products)>0) )
			<section class="probootstrap-section probootstrap-bg-white ">
			  <div class="container">
				<div class="row">
				  <div class="col-md-6 col-md-offset-3 text-center section-heading">
					<h2>Our Selected Projects</h2>
				  </div>
				</div>
			  </div>
		
			  <div class="owl-carousel owl-work">
				  @foreach($products as $k => $product)
					<div class="item">
					  <a href="{{route('portfolioShow',[$product->id])}}">
						<img src="{{ asset(env('THEME'))}}/img/work/{{ $product->image}}" alt="{{ $product->title}}">
					  </a>
					</div> 
				  @endforeach     
			  </div>
			</section>   
		@endif	
	<!-- END Products Portfolio --> 