
    <!-- Section Slides Top Product -->
    
    @if(isset($product) && is_object($product) && (count($product)>0) )
		<section class="flexslider">
			<ul class="slides">
				
					<li style="background-image: url({{ asset(env('THEME'))}}/img/work/{{ $product->image}})" class="overlay">
						<div class="container">
							<div class="row">
							  <div class="col-md-8 col-md-offset-2">
								 <div class="probootstrap-slider-text text-center">
									<h1 class="probootstrap-heading">{{ $product->title}}</h1>
									<p class="probootstrap-subheading">{{ $product->description}}</p>
								  </div>
							  </div>
							</div>
						</div>
					</li>
				  
			</ul>
		</section>
	@endif
	 <!-- END Section Slides Top -->


