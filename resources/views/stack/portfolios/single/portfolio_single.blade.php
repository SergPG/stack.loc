@extends(env('THEME').'.layouts.index')

	<!-- Header -->
		@section('slides_top')
			<!--  Slides Top -->
				@include(env('THEME').'.portfolios.single.slides_top_single')
			<!-- END Slides Top -->
		@endsection
	<!-- END Header -->	

@section('content') 
    
  @if(isset($product) && is_object($product) && (count($product)>0) )

    <section class="probootstrap-section proboostrap-clients probootstrap-bg-white probootstrap-zindex-above-showcase">
      <div class="container">

        <div class="row mb50">
          <div class="col-md-4"><h1 class="mt0">{{ $product->title}}</h1></div>
          <div class="col-md-7 col-md-push-1">
				{!! $product->text!!} 
          </div>
        </div>
        <!-- END row -->
        <div class="row">
          <div class="col-md-12">
            <p><img src="{{ asset(env('THEME'))}}/img/work/{{ $product->image}}" class="img-responsive" alt="{{ $product->title}}" style="width:100%;"></p>
          </div>
        </div>
      </div>
    </section>
@endif	
		
	
		@section('testimonial') 
		   @include(env('THEME').'.layouts.testimonial')  		 	
		@show
		
		@section('portfolio_products') 
		   	@include(env('THEME').'.portfolios.single.portfolio_products') 		 	
		@show
		
		@section('section_bottom') 
		   @include(env('THEME').'.layouts.section_bottom')  		 	
		@show

@endsection

		



