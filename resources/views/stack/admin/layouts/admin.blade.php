<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>SB Admin 2 - Bootstrap Admin Theme</title>

    <!-- Bootstrap Core CSS -->
    <link href="{{ asset(env('THEME').'/admin')}}/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
 
    <!-- MetisMenu CSS -->
    <link href="{{ asset(env('THEME').'/admin')}}/vendor/metisMenu/metisMenu.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="{{ asset(env('THEME').'/admin')}}/dist/css/sb-admin-2.css" rel="stylesheet">

    <!-- Morris Charts CSS -->
    <link href="{{ asset(env('THEME').'/admin')}}/vendor/morrisjs/morris.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="{{ asset(env('THEME').'/admin')}}/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    
	
	

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

    <div id="wrapper">
          @section('navigation')			
			<!--    Navigation   -->
				@include(env('THEME').'.admin.layouts.main_navigation')
			<!-- END Navigation -->
		  @show
        
				
		<!--   Page Content   -->
			<!--   #page-wrapper   -->
			<div id="page-wrapper">
				
				@section('page-header')	
					<!-- page-header -->
					  <div class="row">
						<div class="col-lg-12">
							<h1 class="page-header">Dashboard</h1>
						</div>
						<!--  /.col-lg-12 -->
					  </div>
					<!-- END page-header /.row -->
				@show
				
					
					@yield('content')
				
			</div>
			<!-- END /#page-wrapper -->
		<!--  END Page Content   -->	

    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="{{ asset(env('THEME').'/admin')}}/vendor/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="{{ asset(env('THEME').'/admin')}}/vendor/bootstrap/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="{{ asset(env('THEME').'/admin')}}/vendor/metisMenu/metisMenu.min.js"></script>

    <!-- Morris Charts JavaScript -->
    <script src="{{ asset(env('THEME').'/admin')}}/vendor/raphael/raphael.min.js"></script>
    <script src="{{ asset(env('THEME').'/admin')}}/vendor/morrisjs/morris.min.js"></script>
    <script src="{{ asset(env('THEME').'/admin')}}/data/morris-data.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="{{ asset(env('THEME').'/admin')}}/dist/js/sb-admin-2.js"></script>

</body>

</html>
