


<!--  Services Top --> 
@if(isset($services) && is_object($services)&& (count($services)>0))
     
    <section class="probootstrap-section probootstrap-bg-white probootstrap-zindex-above-showcase">
      <div class="container">
        <div class="row">
          <div class="col-md-6 col-md-offset-3 text-center section-heading probootstrap-animate" data-animate-effect="fadeIn">
            <h2>Our Services</h2>
          </div>
        </div>
        <!-- END row -->
		
		
        <div class="row probootstrap-gutter60">
		  @foreach($services as $k => $service)
          <div class="col-md-4 probootstrap-animate" data-animate-effect="fadeIn">
            <div class="service hover_service text-center">
              <div class="icon"><i class="icon-{{ $service->icon}}"></i></div>
              <div class="text">
                <h3>{{ $service->title}}</h3>
                <p>{{ $service->description}}</p>
              </div>  
            </div>
          </div>
		  @endforeach 
        </div>
        <!-- END row -->
        <div class="row mt50">
          <div class="col-md-12 text-center">
            <a href="{{route('services')}}" class="btn btn-primary btn-lg" role="button">View all our services</a>
          </div>
        </div>
      </div>
    </section>
@endif
<!-- END Services Top  -->   



    
 
    
