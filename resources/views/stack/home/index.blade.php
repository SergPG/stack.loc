@extends(env('THEME').'.layouts.index')

@section('content') 
    
        @section('services_top_main') 
		   @include(env('THEME').'.home.services_top_main')  		 	
		@show

		@section('portfolio_products') 
		   @include(env('THEME').'.home.portfolio_products')  		 	
		@show
		
		@section('clients_company') 
		   @include(env('THEME').'.layouts.clients_company')  		 	
		@show
		
		@section('services_bottom') 
		   @include(env('THEME').'.layouts.services_bottom')  		 	
		@show
		
		@section('testimonial') 
		   @include(env('THEME').'.layouts.testimonial')  		 	
		@show

@endsection

		