

	<!--  Products Portfolio --> 
		@if(isset($products) && is_object($products) && (count($products)>0) )
			<section class="probootstrap-section probootstrap-bg-white ">
			  <div class="owl-carousel owl-work">
				  @foreach($products as $k => $product)
					<div class="item">
					  <a href="{{route('portfolioShow',[$product->id])}}">
						<img src="{{ asset(env('THEME'))}}/img/work/{{ $product->image}}" alt="{{ $product->title}}">
					  </a>
					</div> 
				  @endforeach     
			  </div>
			</section>   
		@endif	
	<!-- END Products Portfolio --> 