
<!-- Our Products  --> 
@if(isset($products) && is_object($products) && (count($products)>0) )
	<section class="probootstrap-section probootstrap-bg-white probootstrap-border-top">
      <div class="container">
        <div class="row">
          <div class="col-md-6 col-md-offset-3 text-center section-heading probootstrap-animate">
            <h2>Our Products</h2>
            <p class="lead">Lorem ipsum dolor sit amet consectetur adipisicing elit. Iusto provident qui tempore natus quos quibusdam soluta at.</p>
          </div>
        </div>
        <!-- END row -->
        <div class="row">
          <div class="col-md-12 probootstrap-animate"  data-animate-effect="fadeIn">

            <div class="owl-carousel owl-carousel-fullwidth border-rounded">
				@foreach($products as $k => $product)
					<div class="item">
					  <a href="{{route('portfolioShow',[$product->id])}}">
						<img src="{{ asset(env('THEME'))}}/img/work/{{ $product->image}}" alt="{{ $product->title}}">
					  </a>
					</div> 
				@endforeach     
            </div>

          </div>

          
        </div>
        <!-- END row -->
      </div>
    </section>
@endif	
<!-- Our Products  -->	