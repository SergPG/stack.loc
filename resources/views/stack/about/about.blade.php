@extends(env('THEME').'.layouts.index')

@section('content') 
    
        @section('main_content') 
		     @include(env('THEME').'.about.main_content') 		 	
		@show
		
		@section('clients_company') 
		   @include(env('THEME').'.layouts.clients_company')  		 	
		@show
		
		@section('portfolio_products') 
		   	@include(env('THEME').'.about.portfolio_products') 		 	
		@show
	
		@section('services_bottom') 
		   @include(env('THEME').'.layouts.services_bottom')  		 	
		@show
		
		@section('testimonial') 
		   @include(env('THEME').'.layouts.testimonial')  		 	
		@show

@endsection

		



