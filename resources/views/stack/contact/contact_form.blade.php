


<!--  Contact Form --> 
  
 <section class="probootstrap-section probootstrap-bg-white">
      <div class="container">
        <div class="row">
          <div class="col-md-5 probootstrap-animate" data-animate-effect="fadeIn">
            <h2>Drop us a line</h2>
				@if (session('status'))
					<div class="alert alert-success">
						{{ session('status') }}
					</div>
			    @endif
			
			
			{!! Form::open(['route' => 'contact','method'=>'post','id'=>'contact_form','class'=>'probootstrap-form']) !!}
				<div class="form-group">
					{!! Form::label('name','Full Name') !!} 
					{!! Form::text('name','',['class'=>'form-control','placeholder' => 'Full Name...']) !!} 
				        @if ($errors->has('name'))
                            <span class="help-block">
                                <strong>{{ $errors->first('name') }}</strong>
                            </span>
                        @endif
				</div>
				<div class="form-group">
					{!! Form::label('email','Email') !!} 
					{!! Form::email('email','',['class'=>'form-control','placeholder' => 'Email...']) !!} 
				</div>
				<div class="form-group">
					{!! Form::label('subject','Subject') !!} 
					{!! Form::text('subject','',['class'=>'form-control','placeholder' => 'Subject...']) !!} 
				</div>
				<div class="form-group">
					{!! Form::label('message','Message') !!} 
					{!! Form::textarea('message','',['cols'=>'30','rows'=>'10','class'=>'form-control','placeholder' => 'Message...']) !!} 
				</div>
				<div class="form-group">
					{!! Form::submit('Submit Form',['name'=>'submit','id'=>'submit','class'=>'btn btn-primary btn-lg']) !!} 
				</div>
				
			{!! Form::close() !!}
						
          </div>
		  
		  
		  
          <div class="col-md-6 col-md-push-1 probootstrap-animate" data-animate-effect="fadeIn">
            <h2>Get in touch</h2>
            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Iusto provident qui tempore natus quos quibusdam soluta at.</p>
            
            <h4>USA</h4>
            <ul class="probootstrap-contact-info">
              <li><i class="icon-pin"></i> <span>198 West 21th Street, Suite 721 New York NY 10016</span></li>
              <li><i class="icon-email"></i><span>info@domain.com</span></li>
              <li><i class="icon-phone"></i><span>+123 456 7890</span></li>
            </ul>
            
            <h4>Europe</h4>
            <ul class="probootstrap-contact-info">
              <li><i class="icon-pin"></i> <span>198 West 21th Street, Suite 721 New York NY 10016</span></li>
              <li><i class="icon-email"></i><span>info@domain.com</span></li>
              <li><i class="icon-phone"></i><span>+123 456 7890</span></li>
            </ul>
          </div>
        </div>
      </div>
    </section>   
  
<!-- END Contact Form  -->   



    
 
    
