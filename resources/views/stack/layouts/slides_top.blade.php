

    <!-- Section Slides Top -->
    
    
    @if(isset($sliders) && is_object($sliders)&& (count($sliders)>0))
		<section class="flexslider">
			<ul class="slides">
				@foreach($sliders as $k => $slider)
					<li style="background-image: url({{ asset(env('THEME'))}}/img/slider/{{ $slider->image}})" class="overlay">
						<div class="container">
							<div class="row">
							  <div class="col-md-8 col-md-offset-2">
								 <div class="probootstrap-slider-text text-center">
									<h1 class="probootstrap-heading">{{ $slider->title}}</h1>
									<p class="probootstrap-subheading">{{ $slider->description}}</p>
								  </div>
							  </div>
							</div>
						</div>
					</li>
				@endforeach     
			</ul>
		</section>
	@endif
	 <!-- END Section Slides Top -->


