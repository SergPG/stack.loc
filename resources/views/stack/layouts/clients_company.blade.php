<!--  Clients Company --> 	
@if(isset($clients) && is_object($clients) && (count($clients)>0) ) 
    <section class="probootstrap-section proboostrap-clients probootstrap-bg-white probootstrap-zindex-above-showcase">
      <div class="container">

        <div class="row">
          <div class="col-md-6 col-md-offset-3 text-center section-heading probootstrap-animate">
            <h2>Big Company Trusts Us</h2>
            <p class="lead">Lorem ipsum dolor sit amet consectetur adipisicing elit. Iusto provident qui tempore natus quos quibusdam soluta at.</p>
          </div>
        </div>
        <!-- END row -->
        
		<div class="row">
		  @foreach($clients as $k => $client)
		   <div class="col-md-3 col-sm-6 col-xs-6 text-center client-logo probootstrap-animate" data-animate-effect="fadeIn">
				<img src="{{ asset(env('THEME'))}}/img/clients/{{ $client->image}}" class="img-responsive" alt="{{ $client->title}}">
			</div>
		    @if($k+1 == (count($clients)/2))
			   <div class="clearfix visible-sm-block visible-xs-block"></div>
			@endif
		  @endforeach               
        </div>
		<!-- END row -->
      </div>
    </section>
@endif	
<!-- END Clients  Company --> 	