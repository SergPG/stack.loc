	 <!-- Section Modal login -->
	<div class="modal fadeInUp probootstrap-animated" id="loginModal" tabindex="-1" role="dialog" aria-labelledby="loginModalLabel" aria-hidden="true">
	
      <div class="vertical-alignment-helper">
        <div class="modal-dialog modal-md vertical-align-center">
          <div class="modal-content">
            
			<button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="icon-cross"></i></button>
            
			<div class="probootstrap-modal-flex">
              <div class="probootstrap-modal-figure" style="background-image: url({{ asset(env('THEME'))}}/img/modal_bg.jpg);"></div>
              
			  <div class="probootstrap-modal-content">
                
				<form id="form_login" action="{{ route('login') }}" class="probootstrap-form" method="POST" >
				 {{ csrf_field() }}
				 
                  <div class="form-group">
                    <input id="email" type="email" name="email" class="form-control" placeholder="Email" value="{{ old('email') }}" required autofocus>
                  </div> 
                  
				  <div class="form-group">
                    <input id="password" type="password" class="form-control" placeholder="Password" name="password" required>
                  </div> 
                  
				  <div class="form-group clearfix mb40">
                    <label for="remember" class="probootstrap-remember">
					  <input type="checkbox" id="remember" name="remember" {{ old('remember') ? 'checked' : '' }}> Remember Me
					</label>
                    <a href="{{ route('password.request') }}" class="probootstrap-forgot">Forgot Password?</a>
                  </div>
				  
                  <div class="form-group text-left">
                    <div class="row">
                      <div class="col-md-6">
                        <input id="form_ok" type="submit" class="btn btn-primary btn-block" value="Sign In">
                      </div>
                    </div>
                  </div>
                  
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>