
<!--  Services Bottom --> 	
    <section class="probootstrap-section">
      <div class="container">
        <div class="row">
		   @foreach($services as $k => $service)
		     @if(($k%(count($services)/2))== 0) <div class="col-md-6"> @endif
				<div class="service left-icon probootstrap-animate" data-animate-effect="fadeInLeft">
				  <div class="icon"><i class="icon-{{ $service->icon}}"></i></div>
				  <div class="text">
					<h3>{{ $service->title}}</h3>
					<p>{{ $service->description}}</p>
				  </div>  
				</div>
		     
		     @if($k+1 == (count($services)/2)) </div> @endif
		   @endforeach           
        </div>
        <!-- END row -->
      </div>
    </section>
	<!-- END Services Bottom --> 