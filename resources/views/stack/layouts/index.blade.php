<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>uiCookies:Stack &mdash; Free Bootstrap Theme, Free Website Template</title>
    <meta name="description" content="Free Bootstrap Theme by uicookies.com">
    <meta name="keywords" content="free website templates, free bootstrap themes, free template, free bootstrap, free website template">
    
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:400,500,700">
    <link rel="stylesheet" href="{{ asset(env('THEME'))}}/css/styles-merged.css">
    <link rel="stylesheet" href="{{ asset(env('THEME'))}}/css/style.min.css">

    <!--[if lt IE 9]>
      <script src="{{ asset(env('THEME'))}}/js/vendor/html5shiv.min.js"></script>
      <script src="{{ asset(env('THEME'))}}/js/vendor/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
	<!-- Header -->	
			@section('navbar')			
			<!-- Fixed navbar -->
				@include(env('THEME').'.layouts.main_navbar')
			<!-- END Fixed navbar -->
			@show
			
			
			
			@section('slides_top')
			<!--  Slides Top -->
				@include(env('THEME').'.layouts.slides_top')
			<!-- END Slides Top -->
			@show
	<!-- END Header -->	
   
	<!-- Content -->	
		@yield('content')
	<!-- END Content -->
	
	<!-- Footer -->
		@section('footer')
		<footer class="probootstrap-footer">
		  <div class="container">
			<div class="row">
			  <div class="col-md-6">
			    <!-- Footer widget -->
					@include(env('THEME').'.layouts.social_footer_widget')
			  </div>
			  <div class="col-md-6">
				<div class="row">
				  <div class="col-md-4">
				    <!-- Footer widget -->
					@include(env('THEME').'.layouts.menu_footer_widget')
				  </div>
				  <div class="col-md-4">
					<!-- Footer widget -->
					@include(env('THEME').'.layouts.menu_footer_widget')
				  </div>
				  <div class="col-md-4">
					<!-- Footer widget -->
					@include(env('THEME').'.layouts.menu_footer_widget')
				  </div>
				</div>
			  </div>
			  
			</div>
			<!-- END row -->
			<div class="row">
			  <div class="col-md-12 copyright">
				<p><small>&copy; 2017 <a href="https://uicookies.com/">uiCookies:Stack</a>. All Rights Reserved. <br> Designed &amp; Developed with <i class="icon icon-heart"></i> by <a href="https://uicookies.com/">uicookies.com</a></small></p>
			  </div>
			</div>
		  </div>
		</footer>
     @show
	 <!-- END Footer -->

	<!-- Modal login -->
		@include(env('THEME').'.layouts.modal_login')  
    <!-- END modal login -->
    
    <!-- Modal signup -->
	    @include(env('THEME').'.layouts.modal_signup')  
    <!-- END modal signup -->
    
       
    <script src="{{ asset(env('THEME'))}}/js/scripts.min.js"></script>
    <script src="{{ asset(env('THEME'))}}/js/custom.min.js"></script>
    <script src="{{ asset(env('THEME'))}}/js/modal_win.js"></script>

  </body>
</html>