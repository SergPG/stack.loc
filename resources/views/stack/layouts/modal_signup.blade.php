	 <!-- Section Modal Sign Up -->
	<div class="modal fadeInUp probootstrap-animated" id="signupModal" tabindex="-1" role="dialog" aria-labelledby="signupModalLabel" aria-hidden="true">
      
	  <div class="vertical-alignment-helper">
        
		<div class="modal-dialog modal-md vertical-align-center">
          
		  <div class="modal-content">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="icon-cross"></i></button>
            <div class="probootstrap-modal-flex">
              <div class="probootstrap-modal-figure" style="background-image: url({{ asset(env('THEME'))}}/img/modal_bg.jpg);"></div>
              <div class="probootstrap-modal-content">
                
				<form method="POST" action="{{ route('register') }}" class="probootstrap-form">
				 
				   {{ csrf_field() }}
				
                  <div class="form-group">
                    <input type="text" id="name" class="form-control" placeholder="Name" name="name" value="{{ old('name') }}" required autofocus>
                  </div> 

				  <div class="form-group">
                    <input id="email" type="email" class="form-control" placeholder="Email" name="email" value="{{ old('email') }}" required>
                  </div> 
				  
                  <div class="form-group">
                    <input type="password" id="password" class="form-control" placeholder="Password" name="password" required>
                  </div> 
				  
                  <div class="form-group">
                    <input type="password" id="password-confirm" class="form-control" placeholder="Re-type Password" name="password_confirmation" required>
                  </div> 
				  
                  <div class="form-group clearfix mb40">
                    <label for="remember" class="probootstrap-remember">
					  <input type="checkbox" id="remember" name="remember" {{ old('remember') ? 'checked' : '' }}> Remember Me
					</label>
                    
                  </div>
                  <div class="form-group text-left">
                    <div class="row">
                      <div class="col-md-6">
                        <input type="submit" class="btn btn-primary btn-block" value="Sign Up">
                      </div>
                    </div>
                    
                  </div>
                  
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>