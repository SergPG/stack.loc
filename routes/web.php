<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'stack\HomeController@index')->name('stack');

/*
|
*/
Route::group(['prefix'=>'about'],function(){
	
	Route::get('/', 'stack\AboutController@index')->name('about');
	
	Route::get('/{person}', 'stack\PersonController@show')->name('personShow');
	
});

/*
|
*/

Route::group(['prefix'=>'portfolios'],function(){
	
	Route::get('/', 'stack\PortfolioController@index')->name('portfolios');
	
	Route::get('/{portfolio}', 'stack\PortfolioController@show')->name('portfolioShow');
	
});

/*
|
*/

Route::group(['prefix'=>'services'],function(){
	
	Route::get('/', 'stack\ServiceController@index')->name('services');
	
	Route::get('/{service}', 'stack\ServiceController@show')->name('servicesShow');
	
});

Route::get('/contact', 'stack\ContactController@index')->name('contact');
Route::post('/contact', 'stack\ContactController@contactUs')->name('contact');;


/**/
Auth::routes();



// Admin
Route::group(['prefix'=>'admin','middleware'=>'auth'],function(){
	//admin
	Route::get('/','admin\IndexController@index')->name('admin');
	

});
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
