<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class MailContactUs extends Mailable
{
    use Queueable, SerializesModels;

    
	protected $name;
	protected $email;
	protected $subjectEmail;
	protected $messageEmail;
	
	/**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct( $name,$email,$subjectEmail,$messageEmail)
    {
        //
		 $this->name = $name;
		 $this->email = $email;
		 $this->subjectEmail = $subjectEmail;
		 $this->messageEmail = $messageEmail;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
	// D:\ospanel\domains\stack.loc\resources\views\emails
        return $this->from($this->email)
					->view('emails.email')->with([
						'name' => $this->name,
						'email' => $this->email,
						'messageEmail' => $this->messageEmail,
						])
					->subject($this->subjectEmail);
    }
}
