<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
		$this->bootRepositories();
    }

	public function bootRepositories()
	{
		$this->app->bind('App\Repositories\ProductRepository', 'App\Repositories\ProductRepositoryFunction');
	}
	
	
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
