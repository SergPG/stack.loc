<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Route; 
use App\Page;

class SiteController extends Controller
{
	protected $positionSidebar = FALSE; // Position Sidebar
	protected $vars = array(); // array variables
	
	protected $routeName;
	protected $routeParameter;
	
	protected $currentPage;
	protected $slidersForPage;

	//----------------------------
	public function __construct(){
		
		
		$this->slidersPege();
		
	}
	//----------------------------
	protected function slidersPege(){
		$this->slidersTopPage = NULL;
		$this->routeName = Route::current()->getName();
		$this->routeParameter = Route::current()->parameters();
		
		if(empty($this->routeParameter)){
		
		}
		
		$this->currentPage = Page::where('alias', $this->routeName)->firstOrFail();
			$this->slidersTopPage = $this->currentPage->sliders;
		
		return $this->slidersTopPage;
		
	}
	
    //
}
