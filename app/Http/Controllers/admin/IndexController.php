<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Product;

class IndexController extends AdminController
{
   
	 //protected $model = FALSE;
	
	
	public function index(){
		$count_products = Product::count();
		
		$model_name = 'App\Product' ;
		$products = $this->getAll($model_name);
		
		//dd($products);
		
		$options = [
			'color' => 'primary',  
	        'icon' => 'briefcase',  
			'title' => 'Портфолио', 
	        'value' => Product::count(), ];
		
		$data = $this->renderInfoPanel($options);
		
		
		//print_r($data);
		 
		 
		return view(env('THEME').'.admin.index.index',[
									'count_products'=>$count_products,
									'data'=>$data,
														]);
		
	}
	
	public function renderInfoPanel( array $options = []){
	  
	  if(empty($options)){
				$options = array_add($options,'color','primary');  
				$options = array_add($options,'icon','comments');  
				$options = array_add($options,'title','New comments!');  
				$options = array_add($options,'value','0');  					
	    }
	
	return $options;		
	}
	
	public function getAll($model){
		$builder = $model::select('*');
	
		return $builder->get();
		
	}
	
	
	
}
