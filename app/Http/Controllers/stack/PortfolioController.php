<?php

namespace App\Http\Controllers\stack;

use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use App\Http\Controllers\SiteController;
use Route; 

use App\Slider;
use App\Service;
use App\Product;
use App\Client;
use App\Page;
 


class PortfolioController extends SiteController
{
    //
	//
	public function index(){
		
		//$page = Page::where('alias', $this->routeName)->first();
		
		$sliders = $this->slidersTopPage;		
		 
		
		$services = Service::all();
		$products = Product::all();   ///Реализовать проверку на пустую БД
		$clients = Client::all();   
		
		
        
      // dd($products);		
		
		return view('stack\portfolios\portfolio',[
								'sliders'=> $sliders,
								'services'=>$services,
								'products'=>$products,
								'clients'=> $clients,
										
										]);

		
	}
	
	//====================================
	public function show($portfolio){
		  
		$product = Product::find($portfolio); 
		$products = Product::where('id','<>',$portfolio)->get();
		
			//dd($products);
		 	
		return view('stack\portfolios\single\portfolio_single',[
								'product'=>$product,
								'products'=>$products,			
										]);
	 
	}
	
	
}
