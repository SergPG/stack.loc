<?php

namespace App\Http\Controllers\stack;

use Illuminate\Http\Request;
use Illuminate\Http\RedirectResponse;
use Illuminate\Routing\Redirector;

use Illuminate\Mail\Mailable;
use Illuminate\Support\Facades\Mail;
use App\Http\Controllers\SiteController;
use Route; 

use App\Mail\MailContactUs;

use App\Slider;
use App\Service;
use App\Product;
use App\Client;
use App\Page;

class ContactController extends SiteController
{
    //
	public function index(){
		
		
		//$page = Page::where('alias', $this->routeName)->first();
		
		$sliders = $this->slidersTopPage;		
		 
		
		$services = Service::all();
		$products = Product::all();   ///Реализовать проверку на пустую БД
		$clients = Client::all();   
		
		
        
      // dd($products);		
		
		return view('stack\contact\contact',[
								'sliders'=> $sliders,
								'services'=>$services,
								'products'=>$products,
								'clients'=> $clients,
										
										]);
		
	}
	
	//
	public function contactUs(Request $request){
		
		$messages = [
			'required' => 'Поле :attribute обязательно к заполнению',
			'email' => 'Поле :attribute должно ссответствовать Е-mail адресу',
		]; 
		
		if($request->isMethod('post')){
			
			$this->validate($request,[
				'name' => 'required|max:255',
				'email' => 'required|email',
				'subject' => 'required|max:255',
				'message' => 'required',
			], $messages);
			
			//print_r($_POST);
			$data = $request->all();
			// dd($data);
			$mail_admin = env('MAIL_ADMIN');
			
			$result = Mail::to($mail_admin)->send(new MailContactUs($data['name'],
											 $data['email'],
											 $data['subject'],
											 $data['message']));
	
		}
	
	   
		 //dd($result);
		if(is_null($result)){
			return redirect()->route('contact')->with('status', 'Сообщение отправлено!');
			
		} 
	}
}
