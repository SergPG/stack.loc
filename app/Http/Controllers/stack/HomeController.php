<?php

namespace App\Http\Controllers\stack;

use Illuminate\Http\Request;
use App\Http\Controllers\SiteController;

use App\Repositories\ProductRepository;

use App\Slider;
use App\Service;
use App\Product;
use App\Client;
use App\Page;
 
use Route; 

class HomeController extends SiteController
{
	
	
	protected $p_rep;
   
    public function __construct(ProductRepository $p_rep)
	{
		parent::__construct();
		
		$this->p_rep = $p_rep;
	}

	
    //
	public function index(){
		
        $comp = $this->p_rep->all();
		
		//dd($comp);
		
		
		//$name = Route::currentRouteName();
		
		//$page = Page::where('alias', $this->routeName)->first();
		
		$sliders = $this->slidersTopPage;		
    
     	
		 
	
     	
		$services = Service::all();
		$products = Product::all();   ///Реализовать проверку на пустую БД
		$clients = Client::all();   
		
	/*	
        
      // dd($products);		*/
		
		return view('stack\home\index',[
								'sliders'=> $sliders,
								'services'=>$services,
								'products'=>$products,
								'clients'=> $clients,
										
										]);
	
	}
	
}
