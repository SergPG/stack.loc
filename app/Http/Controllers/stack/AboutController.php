<?php

namespace App\Http\Controllers\stack;

use Illuminate\Http\Request;

use App\Http\Controllers\SiteController;
use Route; 

use App\Slider;
use App\Service;
use App\Product;
use App\Client;
use App\Page;
 



class AboutController extends SiteController
{
    //
	
	public function index(){
		
		
		
		//$page = Page::where('alias', $this->routeName)->first();
		
		$sliders = $this->slidersTopPage;		
		 
		
		$services = Service::all();
		$products = Product::all();   ///Реализовать проверку на пустую БД
		$clients = Client::all();   
		
		
        
      // dd($products);		
		
		return view('stack\about\about',[
								'sliders'=> $sliders,
								'services'=>$services,
								'products'=>$products,
								'clients'=> $clients,
										
										]);
	}
	
}
