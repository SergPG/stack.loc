<?php

namespace App\Repositories;

use App\Product;
use Config;

class ProductRepositoryFunction implements  ProductRepository
{
	protected $ProductModal;
	
	public function __construct(Product $ProductModal)
	{
		$this->ProductModal = $ProductModal;
	}
	
	public function all($columns = array('*'))
	{
		return $this->ProductModal->all($columns);
	}
}

